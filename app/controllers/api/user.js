var seq = require('sequelize');

module.exports = {

    list: function (req, res) {
        seq.db.models.Student.findAll({
            limit: 20
        }).then(function (users) {
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify(users));
        });
    }

};
