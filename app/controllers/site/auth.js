var seq = require('sequelize');

module.exports = {
    login: function(req,res){
        res.render('auth/login');
    },

    logout: function(req,res){
      res.redirect('/main/index');
    },

    register: function (req, res) {
        res.render('auth/register');
    },

    recoverPass: function (req, res) {
        res.render('auth/forgot-password');
    }

};
