var seq = require('sequelize');

// Just dummy data for test
var data = {title: '8. IZLAZAK - Instrukcije'};


module.exports = {
    index: function (req, res) {
        res.render('main/index', data);
    },

    about: function (req, res) {
        res.render('main/about', data);
    },

    metodologija: function (req, res) {
        res.render('main/metodologija', data);
    },


    predmeti: function (req, res) {
        res.render('main/predmeti', data);
    },

    termsConditions: function (req, res) {
        res.render('main/terms-and-conditions', data);
    },

    dbTest: function (req, res) {

        seq.db
        .sync({ force: true })
        .then(function () {
            seq.db.models.Student.create({
                name: 'Mirko',
                surname: 'Mirkovic',
                dob: new Date(),
                city: 'Zagreb',
                address: 'adsfds 23s',
                driversLicence: false
            });
            res.send('Successfuly written to database!');
        }, function (err) {
            console.log('An error occurred while repopulating table:', err);
            res.send('Error ocurred!');
        });
    },

    react: function (req, res) {
        res.render('main/react', data);
    }
};
