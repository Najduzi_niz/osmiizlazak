module.exports = function (sequelize, types) {
    return sequelize.define('PrijavaPrakse', {
        studentID_FK: types.INTEGER,
        firmaID_FK: types.INTEGER,
        potrebnoSati: types.INTEGER,
        satiTjedno: types.INTEGER,
        pocetak: types.DATE,
        kraj: types.DATE
    });
};
