module.exports = function (sequelize, types) {
    return sequelize.define('OdobrenaPraksa', {
        IDpraksa: {
      type: types.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
        studentID_FK: types.INTEGER,
        firmaID_FK: types.INTEGER,
        status: types.BOOLEAN,
        pocetak: types.DATE,
        kraj: types.DATE
    });
};
