module.exports = function (sequelize, types) {
    return sequelize.define('Smjer', {
        IDsmjer: {
      type: types.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
        naziv: types.STRING,
        ustanovaID_FK: types.INTEGER
    });
};
