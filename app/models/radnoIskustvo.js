module.exports = function (sequelize, types) {
    return sequelize.define('RadnoIskustvo', {
        IDradnoIskustvo: {
      type: types.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
        firma: types.STRING,
        radnoMjesto: types.STRING,
        grad: types.STRING,
        opis: types.STRING,
        pocetak: types.DATE,
        kraj: types.DATE
    });
};
