//TODO: u srijedu cu imati vise informacija o ovom
module.exports = function (sequelize, types) {
    return sequelize.define('Recenzija', {
        IDrecenzija:{
      type: types.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
        //name: types.STRING,
        //surname: types.STRING,
        //dob: types.DATE,
        Ocjena: types.STRING,
        firmaID_FK: types.INTEGER,
        studentID_FK: types.INTEGER
    });
};
