module.exports = function (sequelize, types) {
    return sequelize.define('Student', {
        IDstudent:{
      type: types.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
        ime: types.STRING,
        prezime: types.STRING,
        email: types.STRING,
        user: {
          type: types.STRING,
          allowNull: false,
          unique: true
        },
        pass: {
          type: types.STRING,
          allowNull: false,
          unique: true
        }
    });
};
