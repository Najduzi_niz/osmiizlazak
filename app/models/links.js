module.exports = function (sequelize, types) {
    return sequelize.define('Links', {
        IDlinks: {
      type: types.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
        url: types.STRING,
        studentID_FK: types.INTEGER
    });
};
