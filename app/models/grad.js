module.exports = function (sequelize, types) {
    return sequelize.define('Grad', {
        IDgrad: {
      type: types.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
        naziv: types.STRING
    });
};
