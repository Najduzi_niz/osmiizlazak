module.exports = function (sequelize, types) {
    return sequelize.define('Linkf', {
        IDlinkf: {
      type: types.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
        url: types.STRING,
        firmaID_FK: types.INTEGER
    });
};
