module.exports = function (sequelize, types) {
    return sequelize.define('Korisnik', {
        IDkorisnik:{
      type: types.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
        user: {
          type: types.STRING,
          allowNull: false,
          unique: true
        },
        pass: {
          type: types.STRING,
          allowNull: false,
          unique: true
        }
    });
};
