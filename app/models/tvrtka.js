module.exports = function (sequelize, types) {
    return sequelize.define('Tvrtka', {
        IDtvrtka: {
      type: types.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
        naziv: types.STRING,
        adresa: types.STRING,
        LinkFirmaID_FK: types.INTEGER,
        opis: types.STRING,
        gradID_FK: types.INTEGER,
        brojMjesta: types.INTEGER,
        email: types.STRING,
        ikona: types.STRING
    });
};
