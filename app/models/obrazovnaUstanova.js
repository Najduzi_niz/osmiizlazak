module.exports = function (sequelize, types) {
    return sequelize.define('ObrazovnaUstanova', {
        IDustanova: {
      type: types.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
        status: {type: types.BOOLEAN, allowNull: false, defaultValue: false},
        studentID_FK: types.INTEGER,
        firmaID_FK: types.INTEGER
        });
};
