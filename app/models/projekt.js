module.exports = function (sequelize, types) {
    return sequelize.define('Projekt', {
        IDprojekt: {
      type: types.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
        naziv: types.STRING,
        radnoMjesto: types.STRING,
        opis: types.STRING,
        pocetak: types.DATE,
        kraj: types.DATE
    });
};
