var main = require('../app/controllers/site/main'),
    auth = require('../app/controllers/site/auth'),
    user = require('../app/controllers/api/user');

module.exports = function (next) {
    var app = this.app;

    // Static pages routes
    app.get('/', main.index);
    app.get('/o-nama', main.about);
    app.get('/metodologija', main.metodologija);
    app.get('/predmeti', main.predmeti);
    app.get('/uvjeti-koristenja', main.termsConditions);
    app.get('/dbtest', main.dbTest);
    app.get('/reacttest', main.react);

    // Auth routes
    app.get('/login', auth.login);
    app.get('/register', auth.register);
    app.get('/forgot-password', auth.recoverPass);
    app.get('/logout', auth.logout);

    // API routes for web app
    app.get('/api/v1/', user.list);

    next();
};
