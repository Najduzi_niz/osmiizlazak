var path = require('path'),
    _ = require('lodash');

var environment = process.env.NODE_ENV || 'develop',
    defaults = {
        root: path.normalize(path.join(__dirname, '..')),
        port: 8080,
        publicDir: 'public',
        viewsDir: path.normalize('app/views'),
        modules: [
            'express',
            'routes',
            'database'
        ]
    };

var conf = {};

try {
    conf = require(path.join(__dirname, 'env', environment + '.json'));
} catch (e) {
    console.log(e);
}

module.exports = _.assign(defaults, conf);
