var fs = require('fs')
  , path = require('path')
  , Sequelize = require('sequelize')
  , _ = require('lodash');

module.exports = function (next) {

    var conf = this.config
      , modelsPath = path.join(conf.root, 'app/models')
      , sequelize
      , db = {};

    sequelize = new Sequelize(
            conf.dbName,
            conf.dbUser,
            conf.dbPass ,{
                host: conf.dbHost,
                dialect: 'mysql'
    });

    fs.readdirSync(modelsPath)
      .forEach(function (file) {
           var m = sequelize.import(path.join(modelsPath, file));
           db[m.name] = m;
      });

    Object.keys(db).forEach(function (mName) {
        if ( db[mName].options.hasOwnProperty('associate') ) {
            db[mName].options.associate(db);
        }
    });

    // TODO: review this; possiby very bad practice!
    // assigning db instance to cached module for easier access
    // from controllers and similar.
    // Maybe try to do this by binding controller functions in routes.js
    Sequelize.db = sequelize;

    next();
};
