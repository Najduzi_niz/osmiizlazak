var express = require('express')
var path = require('path');
var session =	require('express-session');
var bodyParser = require('body-parser');


module.exports = function (next) {
    var app = this.app = express()
      , config = this.config;

    app.use('/static/', express.static(path.join(config.root, config.publicDir)));
    app.use(session({secret: 'ssshhhhh',saveUninitialized: true,resave: true}));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true}));
    app.set('view engine', 'jade');
    app.set('views', path.join(config.root, config.viewsDir));

    // Open socket on port
    var server = app.listen(config.port, function (){
        console.log('Server started on port %s in %sms',
            config.port,
            config.bootTime
        );
    });

    next();
};
