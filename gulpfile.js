var gulp = require('gulp')
  , autoprefixer = require('gulp-autoprefixer')
  , sass = require('gulp-sass')
  , browserify = require('gulp-browserify')
  , uglify = require('gulp-uglify')
  , minify = require('gulp-minify-css')
  , lr = require('gulp-livereload')
  , bower = require('gulp-bower-files')
  , gulpFilter = require('gulp-filter')
  , concat = require('gulp-concat');

gulp.task('styles', function () {
    return gulp.src('public/src/sass/style.scss')
            .pipe(sass())
            .pipe(minify())
            .pipe(lr())
            .pipe(gulp.dest('public/build/css/'));
});

gulp.task('scripts', function() {
    return gulp.src('public/src/js/main.js')
          .pipe(browserify({}))
          .pipe(uglify())
          .pipe(lr())
          .pipe(gulp.dest('public/build/js/'));
});

gulp.task('views', function () {
    return gulp.src('app/views/**/*.jade')
            .pipe(lr());
});

gulp.task('bower', function () {
    var cssFilter = gulpFilter('**/*.css')
      , jsFilter = gulpFilter('**/*.js');

    return bower()
        .pipe(cssFilter)
        .pipe(concat('components.min.css'))
        .pipe(minify())
        .pipe(gulp.dest('public/build/css/'))
        .pipe(cssFilter.restore())
        .pipe(jsFilter)
        .pipe(concat('components.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('public/build/js/'));
});

gulp.task('watch', function() {
    lr.listen();
    gulp.watch('public/src/js/**/*.js', ['scripts']);
    gulp.watch('public/src/sass/**/*.scss', ['styles']);
    gulp.watch('app/views/**/*.jade', ['views']);
});

gulp.task('default', ['watch']);
gulp.task('build', ['scripts', 'styles']);
