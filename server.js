var conf = require('./config')
    boot = require('./lib/boot');

// Bootstraps the application
boot(conf, function (err) {
    if ( err ) {
        console.log('Something is broken!\n%s', err);
    }
});
