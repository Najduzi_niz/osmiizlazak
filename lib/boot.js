var _ = require('lodash');

module.exports = function (config, cb) {
    var modules = config.modules
      , count = 0
      , ctx = {config: config}
      , startTime = Date.now()
      , timeDiff
      , next = function (err) {
            if ( err ) return cb(err);
            if ( ++count < modules.length ) {
                load(modules[count], next);
            }
      };

    var m;

    function load (module, next) {
        try {
            m = require('../config/' + module);
            m.call(ctx, next);
        } catch (err) {
            next(err);
        }
    }

    load(modules[count], next);
    timeDiff = Date.now() - startTime;
    cb.call(_.assign(config,{
        bootTime: timeDiff
    }));
};
